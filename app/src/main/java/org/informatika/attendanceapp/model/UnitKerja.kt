package org.informatika.attendanceapp.model

import android.graphics.Color
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.PolylineOptions
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.PropertyName

data class UnitKerja (
    var level: String? = null,
    var nama: String? = null,
    var parent: DocumentReference? = null,

    @get:PropertyName("batas_wilayah")
    @set:PropertyName("batas_wilayah")
    @PropertyName("batas_wilayah")
    var batasWilayah: List<Wilayah>? = null,
) {

    override fun toString(): String {
        return "level : $level, nama : $nama, batas_wilayah : $batasWilayah"
    }

    fun batasWilayahInPolyLineOptions(): List<PolylineOptions>? {
        if (batasWilayah==null) return null

        var polylineOptionsList = mutableListOf<PolylineOptions>()

        for (w in batasWilayah!!) {
            var polylines = mutableListOf<LatLng>()
            for (p in w.polygons!!) {
                val latLng = LatLng(p.latitude, p.longitude)
                polylines.add(latLng)
            }

            val polylineOptions = PolylineOptions().addAll(polylines).color(Color.RED).width(5f)
            polylineOptionsList.add(polylineOptions)
        }

        return polylineOptionsList
    }

    fun batasWilayahInPolygons() : List<List<LatLng>>? {
        if (batasWilayah==null) return null
        var polygons = mutableListOf<List<LatLng>>()

        for (w in batasWilayah!!) {
            var polygon = mutableListOf<LatLng>()
            for (p in w.polygons!!) {
                val latLng = LatLng(p.latitude, p.longitude)
                polygon.add(latLng)
            }

            polygons.add(polygon)
        }

        return polygons
    }

    companion object {
        const val COLLECTION_NAME = "unit_kerja"
    }
}