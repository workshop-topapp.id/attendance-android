package org.informatika.attendanceapp.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import org.informatika.attendanceapp.R
import org.informatika.attendanceapp.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var mAuth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mAuth = FirebaseAuth.getInstance()
        if (mAuth.currentUser!=null) moveToTakeAttendaceActivity()

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.buttonLogin.setOnClickListener {
            try {
                if (!isFormValidate()) throw Exception("Form can't be empty")
                val username = binding.etUsername.text.toString()
                val password = binding.etPassword.text.toString()
                mAuth.signInWithEmailAndPassword(username, password)
                    .addOnSuccessListener {
                        showSnackbar("Login successful, welcome ${it.user?.email} ", Snackbar.LENGTH_LONG)
                        moveToTakeAttendaceActivity()
                    }.addOnFailureListener {
                        showSnackbar("Login failed : ${it.message}", Snackbar.LENGTH_LONG)
                        Log.e("MainActivity", "signInWithEmailAndPassword : ", it)
                    }
            } catch (e: Exception){
                showSnackbar(e.message.toString(), Snackbar.LENGTH_SHORT)
            }
        }
    }

    private fun moveToTakeAttendaceActivity() {
        val i = Intent(this, TakeAttendanceActivity::class.java)
        startActivity(i)
    }

    private fun isFormValidate() : Boolean {
        if (binding.etUsername.length() == 0) {
            binding.etUsername.setError("This field is required")
            return false
        }

        if (binding.etPassword.length() == 0) {
            binding.etPassword.setError("This field is required")
            return false
        }

        return true
    }

    private fun showSnackbar(message: String, length: Int) {
        Snackbar.make(
            findViewById(R.id.activity_main),
            message,
            length
        ).show()
    }
}