package org.informatika.attendanceapp.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.android.gms.tasks.Task
import kotlinx.coroutines.launch
import org.informatika.attendanceapp.model.Presence
import org.informatika.attendanceapp.model.Staff
import org.informatika.attendanceapp.model.UnitKerja
import org.informatika.attendanceapp.util.FirebaseService

class TakeAttendanceViewModel : ViewModel() {
    private val _presensi = MutableLiveData<Presence?>(null)
    val presensi: LiveData<Presence?> = _presensi

    private val _unitKerja = MutableLiveData<UnitKerja?>(null)
    val unitKerja: LiveData<UnitKerja?> = _unitKerja

    private val _staff = MutableLiveData<Staff?>(null)
    val staff: LiveData<Staff?> = _staff

    private val _divisi = MutableLiveData<UnitKerja?>(null)
    val divisi: LiveData<UnitKerja?> = _divisi

    fun presensiMasuk(presence: Presence): Task<Void> {
        return FirebaseService.addPresence(presence)
            .addOnFailureListener {
            Log.e(
                "TakeAttendanceViewModel",
                "Error presensiMasuk",it
            )
        }
    }

    fun presensiPulang(presence: Presence){
        FirebaseService.addPresence(presence)
            .addOnFailureListener {
            Log.e(
                "TakeAttendanceViewModel", "Error presensiPulang",it
            )
        }
    }

    init {

        viewModelScope.launch {
            Log.d("TakeAttendanceViewModel", "launch")
            _presensi.value = FirebaseService.getPresence()

            FirebaseService.getUser().collect{ staff ->
                Log.d("TakeAttendanceViewModel", "getUser()")
                _staff.value = staff

                _divisi.value = FirebaseService
                    .getUserUnitKerja(staff!!.unitKerja!!)

                Log.d("TakeAttendanceViewModel", "_divisi : ${_divisi.value}")

                if (divisi.value!=null && divisi.value?.parent!=null){
                    _unitKerja.value = FirebaseService
                        .getUserUnitKerja(
                            divisi.value!!.parent!!
                        )
                }
            }
        }
    }
}